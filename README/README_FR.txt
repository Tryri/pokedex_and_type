Fait par:
      _____                    _____                _____                    _____                    _____          
     /\    \                  /\    \              |\    \                  /\    \                  /\    \         
    /::\    \                /::\    \             |:\____\                /::\    \                /::\    \        
    \:::\    \              /::::\    \            |::|   |               /::::\    \               \:::\    \       
     \:::\    \            /::::::\    \           |::|   |              /::::::\    \               \:::\    \      
      \:::\    \          /:::/\:::\    \          |::|   |             /:::/\:::\    \               \:::\    \     
       \:::\    \        /:::/__\:::\    \         |::|   |            /:::/__\:::\    \               \:::\    \    
       /::::\    \      /::::\   \:::\    \        |::|   |           /::::\   \:::\    \              /::::\    \   
      /::::::\    \    /::::::\   \:::\    \       |::|___|______    /::::::\   \:::\    \    ____    /::::::\    \  
     /:::/\:::\    \  /:::/\:::\   \:::\____\      /::::::::\    \  /:::/\:::\   \:::\____\  /\   \  /:::/\:::\    \ 
    /:::/  \:::\____\/:::/  \:::\   \:::|    |    /::::::::::\____\/:::/  \:::\   \:::|    |/::\   \/:::/  \:::\____\
   /:::/    \::/    /\::/   |::::\  /:::|____|   /:::/~~~~/~~      \::/   |::::\  /:::|____|\:::\  /:::/    \::/    /
  /:::/    / \/____/  \/____|:::::\/:::/    /   /:::/    /          \/____|:::::\/:::/    /  \:::\/:::/    / \/____/ 
 /:::/    /                 |:::::::::/    /   /:::/    /                 |:::::::::/    /    \::::::/    /          
/:::/    /                  |::|\::::/    /   /:::/    /                  |::|\::::/    /      \::::/____/           
\::/    /                   |::| \::/____/    \::/    /                   |::| \::/____/        \:::\    \           
 \/____/                    |::|  ~|           \/____/                    |::|  ~|               \:::\    \          
                            |::|   |                                      |::|   |                \:::\    \         
                            \::|   |                                      \::|   |                 \:::\____\        
                             \:|   |                                       \:|   |                  \::/    /        
                              \|___|                                        \|___|                   \/____/         
                                                                                                                     
Contenu:

Deux README.txt (Français et Anglais)

Trois fichiers .json: (ATTENTION !! Le contenu des fichiers .json est en français et utilise le système métrique international)

- pokedex.json:  Contiens les données des 802 pokémons actuels:
  - Numero:     Numéro dans pokedex
  - Nom:        Nom du pokemon
  - Type1:      Premier type du pokemon
  - Type2:      Second type du pokemon (vide si il n'en possède pas)
  - PV:         Points de vie du pokemon
  - Attaque:    Attaque du pokemon
  - Defense:    Defense du pokemon
  - Vitesse:    Vitesse du pokemon
  - Taille:     Taille du pokemon (en mètre)
  - Poids:      Poids du pokemon (en kilogramme)
  - Image:      Nom de l'image correspondant au pokemon (Toutes les images sont dans l'archive Image.zip)
  - id:         id du pokemon (En informatique on aime bien les id et puis pour les base de données c'est plus simple d'en avoir déjà un !!)
  - Generation: Génération du pokemon

- simple_relation.json: Contiens les relations entre les types:
  "Type":[Liste des types contre lequel il à l'avantage]

- complete_relation: Contiens les relations complète entre les types:
  "type":{
  "immuniser":[],
  "resiste":[],
  "faible":[],
  "innefficace":[],
  "efficace":[],
  "fort":[]

  note: Si un type est dans la liste d'un autre type alors il est faible contre lui

Une archive format .zip contenant les 802 images des pokemons

Une image représentant les relations entre les types

Un dossier contenant du code .py pour l'édition des fichiers ainsi que le code de scrap