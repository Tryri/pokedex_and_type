Done by:
      _____                    _____                _____                    _____                    _____          
     /\    \                  /\    \              |\    \                  /\    \                  /\    \         
    /::\    \                /::\    \             |:\____\                /::\    \                /::\    \        
    \:::\    \              /::::\    \            |::|   |               /::::\    \               \:::\    \       
     \:::\    \            /::::::\    \           |::|   |              /::::::\    \               \:::\    \      
      \:::\    \          /:::/\:::\    \          |::|   |             /:::/\:::\    \               \:::\    \     
       \:::\    \        /:::/__\:::\    \         |::|   |            /:::/__\:::\    \               \:::\    \    
       /::::\    \      /::::\   \:::\    \        |::|   |           /::::\   \:::\    \              /::::\    \   
      /::::::\    \    /::::::\   \:::\    \       |::|___|______    /::::::\   \:::\    \    ____    /::::::\    \  
     /:::/\:::\    \  /:::/\:::\   \:::\____\      /::::::::\    \  /:::/\:::\   \:::\____\  /\   \  /:::/\:::\    \ 
    /:::/  \:::\____\/:::/  \:::\   \:::|    |    /::::::::::\____\/:::/  \:::\   \:::|    |/::\   \/:::/  \:::\____\
   /:::/    \::/    /\::/   |::::\  /:::|____|   /:::/~~~~/~~      \::/   |::::\  /:::|____|\:::\  /:::/    \::/    /
  /:::/    / \/____/  \/____|:::::\/:::/    /   /:::/    /          \/____|:::::\/:::/    /  \:::\/:::/    / \/____/ 
 /:::/    /                 |:::::::::/    /   /:::/    /                 |:::::::::/    /    \::::::/    /          
/:::/    /                  |::|\::::/    /   /:::/    /                  |::|\::::/    /      \::::/____/           
\::/    /                   |::| \::/____/    \::/    /                   |::| \::/____/        \:::\    \           
 \/____/                    |::|  ~|           \/____/                    |::|  ~|               \:::\    \          
                            |::|   |                                      |::|   |                \:::\    \         
                            \::|   |                                      \::|   |                 \:::\____\        
                             \:|   |                                       \:|   |                  \::/    /        
                              \|___|                                        \|___|                   \/____/         
                                                                                                                                                                                                                            
Contents:

Two README.txt (French and English)

Two .json files: (WARNING!!!) The content of the .json files is in French and uses the international metric system)

- pokedex.json: Contains the data of the current 802 pokemons:
  - Number: Number in pokedex.
  - Name: Pokemon name
  - Type1: First type of the pokemon
  - Type 2:Second type of the pokemon (empty if it doesn't have one)
  - VP: Pokemon hit points
  - Attack: Pokemon attack
  - Defense: Pokemon Defense
  - Speed:    Pokemon speed
  - Size: Pokemon size (in meters)
  - Weight: Weight of the pokemon (in kilograms)
  - Image: Name of the image corresponding to the pokemon (All images are in the Image.zip archive)
  - id: id of the pokemon (In computer science we like id and for databases it's easier to have one already !!!)
  - Generation: Generation of the pokemon

- relation_type: Contains the relations between types:
  "Type": [List of types over which the pokemon has an advantage]

- complete_relation: Contains the full relations between types:
  "type":{
  "immuniser":[],
  "resiste":[],
  "faible":[],
  "innefficace":[],
  "efficace":[],
  "fort":[]

  note: If a type is in the list of another type then it is weak against him.

A .zip archive containing the 802 images of the pokemons.

An image of pokemon type effectiveness and weakness

An folder containing .py code for editing files and the scrap code