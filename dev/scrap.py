import requests
import bs4
import json

token = "https://www.pokemontrash.com/pokedex/liste-pokemon.php"
token_details = "https://www.pokemontrash.com/pokedex/"
token_details_2 = "https://www.pokemon.com/fr/pokedex/"

liste_pokemons_data = []
liste_noms_trash = []
liste_noms_data = []
liste_carac_data = []
liste_nombre = ["0","1","2","3","4","5","6","7","8","9"]
liste_number_attributs_data = []
dict_elem_number = {"PV":0,"Attaque":1,"Défense":2,"Att. Spé":3,"Def. Spé":4,"Vitesse":5}
dict_elem_carac = {"Taille":0,"Poids":1}

def recuperation_lien_scraping_trash(elem):
    elem = elem[22:]
    lien = ""
    for lettre in elem:
        if lettre != '"':
            lien += lettre
        else:
            return lien

def get_num(elem):
    res = ""
    for e in elem:
        if e in liste_nombre:
            res = res + e
            if len(res)==3:
                return res
    return res

def get_num_data(elem):
    res = get_num(elem)
    if len(res) == 1:
        res = "00" + res
    elif len(res) == 2:
        res = "0" + res
    return res

def get_nom_data(elem):
    nom_data = elem[:len(elem)-5]
    nom_data = nom_data[len(get_num(nom_data))+1:]
    nom_data = nom_data.replace("--", "-")
    nom_data = nom_data.replace("_", "")
    if nom_data[len(nom_data)-1] == "-":
        nom_data = nom_data[:len(nom_data)-1]
    return nom_data

def get_nom_scrap(elem):
    return get_nom_data(elem).lower()

def extract_data(elem):
    res = []
    for lien in elem:
        lien = str(lien)
        lien = lien[:len(lien)-6]
        lien = lien[33:]
        cpt = 0
        for l in lien:
            cpt += 1
            if l == ">":
                lien = lien[cpt:]
                break
        res.append(lien)
    return res

def extract_data_scrap(elem):
    res_tab = []
    for lien in elem:
        res = ""
        lien = str(lien)
        lien = lien.replace(",",'.')
        lien = lien[:len(lien)-7]
        lien = lien[30:]
        for l in lien:
            if l != " ":
                res += l
            else:
                res_tab.append(res)
                break
        if len(res_tab) == 2:
            return res_tab

def extract_name_evol(elem):
    elem = str(elem)
    elem = elem[:49]
    elem = elem[41:]
    return elem

def extract_type(elem):
    elem = str(elem)
    elem = elem.replace('<span class="type ', "")
    elem = elem.replace('id="pokemon-types"', "")
    elem = elem.replace('<', "")
    elem = elem.replace('>', "")
    elem = elem.replace('/', "")
    elem = elem.replace('<', "")
    elem = elem.replace('div', "")
    elem = elem.replace('span', " ")
    elem = elem.replace('"', "")
    elem = elem.replace(" ", "")
    elem = elem.replace("\r", "")
    elem = elem.replace("]", "")
    elem = elem.replace("[", "")
    elem = elem.replace("\n", "")
    types = []
    mot = ""
    for l in elem:
        # print(l == l.upper(), l)
        if l == l.upper():
            if mot not in types:
                types.append(mot)
            mot = l
        else:
            mot += l

    del types[0]
    if len(types) == 1:
        types.append("")
    return types

def compilation(data_trash, types ,attribue, carac):
    numero = get_num_data(data_trash)
    nom = get_nom_data(data_trash)
    Type1 = types[0]
    Type2 = types[1]
    PV = attribue[dict_elem_number["PV"]]
    Attaque = attribue[dict_elem_number["Attaque"]]
    Def = attribue[dict_elem_number["Défense"]]
    Vitesse = attribue[dict_elem_number["Vitesse"]]
    Taille = carac[dict_elem_carac["Taille"]]
    Poids = carac[dict_elem_carac["Poids"]]
    res = {"Numéro":int(numero),
         "Nom":nom,
         "Type1":Type1,
         "Type2":Type2,
         "PV":int(PV),
         "Attaque":int(Attaque),
         "Défense":int(Def),
         "Vitesse":int(Vitesse),
         "Taille":float(Taille),
         "Poids":float(Poids),
         "Image":get_num(data_trash)+".png"}
    return res

def extract_img(elem):
    elem = str(elem)
    elem = elem[39:]
    elem = elem[:len(elem)-4]
    return elem

def get_data(nom_pokemon_trash):
    print(nom_pokemon_trash)
    nom_page = token_details+nom_pokemon_trash
    recuperation_page = requests.get(nom_page)
    reponse_noms = bs4.BeautifulSoup(recuperation_page.text, 'html.parser')
    
    div_class_graphe = reponse_noms.find_all("div", {"class":"graphe"})
    liste_number_attributs_data = extract_data(div_class_graphe)

    
    div_id_pokemon_types = reponse_noms.find_all("div", {"id":"pokemon-types"})
    types = extract_type(div_id_pokemon_types)

    #Stage 2
    nom_pokemon_scrap = get_nom_scrap(nom_pokemon_trash)
    if get_num(nom_pokemon_trash) == '32':
        nom_pokemon_scrap += "-homme"
    elif get_num(nom_pokemon_trash) == '29':
        nom_pokemon_scrap += "-femme"
    nom_page = token_details_2+nom_pokemon_scrap
    recuperation_page = requests.get(nom_page)
    reponse_charac = bs4.BeautifulSoup(recuperation_page.text, 'html.parser')
    
    span_class_attribute_value = reponse_charac.find_all("span", {"class":"attribute-value"})
    del span_class_attribute_value[2]
    if get_num(nom_pokemon_trash) != '772':
        del span_class_attribute_value[3]
    liste_carac_data = extract_data_scrap(span_class_attribute_value)

    img_class_active = reponse_charac.find_all("img", {"class":"active"})
    img = extract_img(img_class_active)
    image_pokemon = requests.get(img).content
    image_file = io.ByteIO(image_pokemon)
    image = Image.open(image_file).convert('RGB')
    image.save("/home/tpinsard/Images/pokemon/", "PNG", quality=85)

    return compilation(nom_pokemon_trash,types,liste_number_attributs_data, liste_carac_data)

def save_data():
    file = open("data.json", "w+")
    file.write("[")
    for i in range(len(liste_pokemons_data)):
        print("Ecriture de "+str(liste_pokemons_data[i]))
        json_file = json.dumps(liste_pokemons_data[i])
    file.write(json_file+",\n")
    file.write("]")
    file.close()

def get_data_debug(nom_pokemon_trash):
    print(nom_pokemon_trash)
    print(get_num(nom_pokemon_trash))
    print(get_num_data(nom_pokemon_trash))

    nom_page = token_details+nom_pokemon_trash
    recuperation_page = requests.get(nom_page)
    reponse_noms = bs4.BeautifulSoup(recuperation_page.text, 'html.parser')
    
    div_class_graphe = reponse_noms.find_all("div", {"class":"graphe"})
    print(div_class_graphe)
    liste_number_attributs_data = extract_data(div_class_graphe)
    print(liste_number_attributs_data)
    
    div_id_pokemon_types = reponse_noms.find_all("div", {"id":"pokemon-types"})
    types = extract_type(div_id_pokemon_types)
    print(types)

    #Stage 2
    nom_pokemon_scrap = get_nom_scrap(nom_pokemon_trash)
    if get_num(nom_pokemon_trash) == '32':
        nom_pokemon_scrap += "-homme"
    elif get_num(nom_pokemon_trash) == '29':
        nom_pokemon_scrap += "-femme"
    print(nom_pokemon_scrap)
    nom_page = token_details_2+nom_pokemon_scrap
    print(nom_page)
    recuperation_page = requests.get(nom_page)
    reponse_charac = bs4.BeautifulSoup(recuperation_page.text, 'html.parser')


    span_class_attribute_value = reponse_charac.find_all("span", {"class":"attribute-value"})
    print(span_class_attribute_value)
    liste_carac_data = extract_data_scrap(span_class_attribute_value)
    print(liste_carac_data)

    img_class_active = reponse_charac.find_all("img", {"class":"active"})
    img = extract_img(img_class_active)
    image_pokemon = requests.get(img).content
    image_file = io.ByteIO(image_pokemon)
    image = Image.open(image_file).convert('RGB')
    image.save("/home/tpinsard/Images/pokemon/", "PNG", quality=85)




recuperation_page = requests.get(token)
print("Page Scrap 1 recupéré")
reponse = bs4.BeautifulSoup(recuperation_page.text, 'html.parser')
a_class_name = reponse.find_all("a", {"class": "name"})
print("Data Class name récupéré")

for i in a_class_name:
    liste_noms_trash.append(recuperation_lien_scraping_trash(str(i)))
print("Nom à Scrap récupérés")

#772
# del liste_noms_trash[:40]

#get_data_debug("772-Type_0.html")

for nom_pokemon_trash in liste_noms_trash:
    liste_pokemons_data.append(get_data(nom_pokemon_trash))

save_data()