import json

d = open("data.json", "r")
d2 = open("pokedex.json", "w+")
d2.write("[")

d1 = json.load(d)
for l in d1["Pokemon"]:
	data = l
	data["Image"]=data["Numero"]+".png"
	if data["id"] <= 151:
		data["Generation"]=1
	elif data["id"] <= 251:
		data["Generation"]=2
	elif data["id"] <= 386:
		data["Generation"]=3
	elif data["id"] <= 493:
		data["Generation"]=4
	elif data["id"] <= 649:
		data["Generation"]=5
	elif data["id"] <= 721:
		data["Generation"]=6
	elif data["id"] <= 802:
		data["Generation"]=7
	data_write = json.dumps(data)
	d2.write(data_write+",\n")
d2.write("]")
d2.close()
d.close()
